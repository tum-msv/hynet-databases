function testcase2mat(path)
%TESTCASE2MAT Prepare MATPOWER test case files for import to *hynet*
%   This script converts MATPOWER test case files on the given path to
%   corresponding MATLAB MAT-files with a MATPOWER 'mpc' struct as a
%   preparation for the import to *hynet*.
%
%   If *hynet* is installed, open a terminal and call
%
%      python -m hynet import -h
%
%   for more information on importing into the *hynet* database format.

current_path = cd;
cd(path);
m_files = dir('./*.m');

for file = m_files'
    filename = regexprep(file.name, '\.m', '');
    mpc = eval(filename);
    file.name = regexprep(file.name, '\.m', '.mat');
    try
        save(file.name, 'mpc', '-v7.3')
    catch
        disp('Error while converting ' + filename + '.m')
    end
end

cd(current_path);
end
