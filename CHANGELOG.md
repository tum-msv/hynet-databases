# Change Log

This file documents all relevant changes to the grid database library.

## [1.5] - 2020-04-23
- Added ``case2383wp_a.db``, ``case2383wp_ha55.db``, ``case2383wp_hc77.db``, ``case2383wp_hr.db``, ``case2383wp_hr_nr.db``, and ``germany2030ha20.db``. Please refer to the dissertation of Matthias Hotz for more information on these models.
- Updated the grid name and description in ``case2383wp_ha.db``.
- Updated ``germany2030nep.db`` by setting the loss price to $0.75/MWh for a sensible utilization of the renewable energy sources with zero marginal cost.
- Updated ``pjm_hybrid.db`` by setting the reactive power capability of the converters to 25% of their active power capability (was 35%).

## [1.4] - 2019-08-19
- Updated ``pjm_adapted.db`` with voltage drop limits of +/- 5%.
- Updated ``pjm_hybrid.db`` by creating the model as described in the tutorial "Converting AC Lines to DC Operation" provided with hynet v1.2.0 (while retaining injector 3 for the "N-1 Security" scenario):
  * The zone of the buses 6 to 8 was changed from 2 to 1.
  * The series resistance of branch 5 and 6 was updated.
  * Voltage drop limits of +/- 5% were added to the branches 1 to 4.
  * Reactive power capability (35% of the active power capability) was added to the converters.
- Added ``case2383wp_ha.db``, which is a MT-HVDC variant of the hybrid system presented in [this paper](https://ieeexplore.ieee.org/document/7997734).

## [1.3] - 2019-06-21
- Updated the [Power Grid Lib](https://github.com/power-grid-lib/pglib-opf) (PGLib) Optimal Power Flow test cases to v19.05.

## [1.2] - 2019-06-17
- Added the test cases of [MATPOWER](https://github.com/MATPOWER/matpower) 7.0b1+ (accessed 2019-06-13)
  * Infinite active/reactive power limits (``Inf``) are set to (+/-) 10 GW/Gvar.
  * Test cases without any generation cost are set to loss minimization with a loss price of $1/MWh.
  * To emulate MATPOWER's simple DC line model, DC lines are imported as AC/AC converters. Lossless DC lines are imposed with a loss factor of 0.1%.
  * In ``case_RTS_GMLC.m``, the cost function of generator 74 (line 516) is nonconvex due to an inaccurate specification of 3 sample points of a linear function. This is fixed by changing the cost at 397.33333 MW from $3219.79067/h to $3219.79062/h.
  * The test cases ``case9Q.m`` and ``case30Q.m`` are excluded as the import of reactive power cost functions is currently not supported.

## [1.1] - 2019-01-10
- Updated the [Power Grid Lib](https://github.com/power-grid-lib/pglib-opf) (PGLib) Optimal Power Flow test cases to v19.01.
- Updated the ramping limits in ``germany2015.db`` and ``germany2030nep.db``.

## [1.0] - 2018-10-19
- Initial release comprising two adaptations of the PJM test system, a model of the German transmission grid in 2015 and a projection thereof for 2030, as well as all test cases of the [Power Grid Lib](https://github.com/power-grid-lib/pglib-opf) (PGLib) Optimal Power Flow v18.08.
