# *hynet* Grid Database Library

This library is a collection of grid databases for *[hynet](https://gitlab.com/tum-msv/hynet)*, an optimal power flow framework for hybrid AC/DC power systems. *hynet* uses [SQLite](https://www.sqlite.org)-based SQL databases to store grid infrastructure and scenario information. This library provides databases for several power grids, including a hybrid AC/DC adaptation of the [PJM test system](https://ieeexplore.ieee.org/document/5589973) and a model of the [German transmission grid](https://ieeexplore.ieee.org/document/8278744/), both with several different scenarios, as well as an import of the [Power Grid Lib (Optimal Power Flow)](https://github.com/power-grid-lib/pglib-opf) and the test cases provided with [MATPOWER](https://github.com/MATPOWER/matpower). For more information on the import of grid data, please refer to the section "[Management of Grid Databases](https://hynet.readthedocs.io/en/latest/usage.html#management-of-grid-databases)" in [USAGE.md](https://gitlab.com/tum-msv/hynet/blob/master/USAGE.md) of *[hynet](https://gitlab.com/tum-msv/hynet)*.


## Contributing

Contributions of grid databases are very welcome. To contribute, please [fork](https://docs.gitlab.com/ce/user/project/repository/forking_workflow.html) this repository, add the new grid databases (using [Git LFS](https://git-lfs.github.com/)), update ``CHANGELOG.md`` with the staged changes, and create a [merge request](https://docs.gitlab.com/ce/user/project/repository/forking_workflow.html#merging-upstream). Please feel free to submit comments and questions using the [issue tracker](https://gitlab.com/tum-msv/hynet-databases/issues) of this project.


## License

The grid databases were either included with permission or converted from publicly available data. For license, background, and attribution information of a particular grid database, please refer to its *description*. To this end, install *[hynet](https://gitlab.com/tum-msv/hynet)*, start a Python shell in the directory of the grid database, and print its description with

```python
import hynet as ht
database = ht.connect('your_selected_database.db')
print(database.description)
```

in which ``your_selected_database.db`` is the file name of the considered grid database.

Alternatively, the database may be browsed directly, e.g., using the [DB Browser for SQLite](http://sqlitebrowser.org/) or [SQLite Online](https://sqliteonline.com/), where the description is found in the table ``db_info``.
